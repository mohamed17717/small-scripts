// download from slider
var fb,
	fb_sliderSlctor = 'div.stageWrapper',
	fb_nxtBtnSlctor = fb_sliderSlctor + ' ' + 'a.next',
	fb_imgSlctor    = fb_sliderSlctor + ' ' + 'img';


var	fb_sliderSlctor = 'div.stageWrapper',
	fb_nxtBtnSlctor = fb_sliderSlctor + ' ' + 'a.snowliftPager.next',
	fb_imgSlctor    = fb_sliderSlctor + ' ' + 'img.spotlight';

var insta,
	insta_sliderSlctor = 'div._sxolz',
	insta_nxtBtnSlctor = insta_sliderSlctor + ' ' + 'a._8kphn._by8kl.coreSpriteRightChevron',
	insta_imgSlctor    = insta_sliderSlctor + ' ' + 'div._4rbun a img';

function downloadTag(url, fileName){
    var a  = document.createElement('a');

    a.href = url;
    a.download = fileName;
    a.style = "display: none";

    document.body.appendChild(a);
    return a;
}
function blobDownload(link, fileName){
    var xhr = new XMLHttpRequest(); 
    xhr.open("GET", link); 
    xhr.responseType = "blob";
    xhr.onload = function() {
        var url   = URL.createObjectURL(xhr.response);
        downloadTag(url, fileName).click()
    }
    xhr.send();
}

function slider_clkNext(nxt_btn){
	document.querySelector(nxt_btn).click()
}

function sliderDownloader(nxtBtnSlctor, imgSlctor){
	imgs = window.imgs? window.imgs : [];
	intrvl = setInterval(function(){
		// get img link
		imgLink = document.querySelector(imgSlctor).src;

		if (  imgs.includes(imgLink)  ){
            imgs = [];
			clearInterval(intrvl);
		}else{
			imgs.push(imgLink);
			imgName = imgLink.split('?')[0].split('/').pop();
			blobDownload(imgLink, imgName);
			console.log(imgs.length, '- ', imgLink); 
			slider_clkNext(nxtBtnSlctor);
		}
	}, 2200) 
}

sliderDownloader(fb_nxtBtnSlctor, fb_imgSlctor);

