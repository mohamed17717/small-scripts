class EloRating:
    
    kFactor = 5

    def __init__(self, rating_1, rating_2):
        self.rating_1 = rating_1
        self.rating_2 = rating_2        

        self.ex_1 = 1/ (1 + (10**( (rating_2-rating_1)/400) )  )
        self.ex_2 = 1/ (1 + (10**( (rating_1-rating_2)/400) )  )

        self.actualWinner = 1
        self.actualLoser  = 0
    
    def result(self, winner):
        if str(winner).lower() in ('one', '1'):
            winnerRating = self.rating_1
            winnerExpect = self.ex_1

            loserRating = self.rating_2
            loserExpect = self.ex_2

        elif str(winner).lower() in ('two', '2'):
            winnerRating = self.rating_2
            winnerExpect = self.ex_2

            loserRating = self.rating_1
            loserExpect = self.ex_1

        
        winner = winnerRating + self.kFactor * (self.actualWinner - winnerExpect)
        loser  = loserRating  + self.kFactor * (self.actualLoser - loserExpect)
        return int(winner), int(loser)

class chalenger:
    def __init__(self, rating):
        self.rating = rating

    def __mul__(self, othr):
        instance = EloRating(self.rating, othr.rating)
        winner = input('winner is: ')
        result = instance.result(winner)
        print('winner: %s\nloser: %s' % result)
        return result
