k = 5

def expectation(rating_1, rating_2):
    ex_1 = 1/ (1 + (10**( (rating_2-rating_1)/400) )  )
    ex_2 = 1/ (1 + (10**( (rating_1-rating_2)/400) )  )

    return round(ex_1, 3), round(ex_2, 3)

def modifyRating(rating, expected, actual = 1, kfactor = k):
    calc =rating + kfactor * (actual - expected)
    return int(calc)

##def lose(rating, expected, actual = 0, kfactor = k):
##    

# kasbarov
rating_1 = 8

# nancy
rating_2 = 18

ex_1, ex_2 = expectation(rating_1, rating_2)
##print(ex_1, ex_2)

print(modifyRating(rating_1, ex_1, 1))
print(modifyRating(rating_2, ex_2, 0))

print(modifyRating(rating_1, ex_1, 0))
print(modifyRating(rating_2, ex_2, 1))
