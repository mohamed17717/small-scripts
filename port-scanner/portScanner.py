import socket
from time import time
start = time()


def scan(ip):
    for port in range (1, 500):
        try:
            s = socket.socket()
            s.settimeout(0.3)
            s.connect((ip, port))
            print ("[+] Port : %s | Service : %s" %( port, socket.getservbyport(port) ) )
        except:
            pass
    print("\n\n[..] Check Completed [..]\n\n")

def takeInput():
    ip = input("[+] Enter IP For Scan: ")
    while not ip :
        ip = input("[+] Enter IP MthrFckr: ")
    return ip

ip = takeInput()
scan(ip)
print("[+] All Tiem Is: ", round(time() - start, 2))
