function timeInSeconds(t){
	t = t.split(':')
	minutes = t[0]
	seconds = t[1]

	return parseInt(minutes) * 60 + parseInt(seconds)

}

function getAllTime(){

    selector = 'span.style-scope.ytd-thumbnail-overlay-time-status-renderer'
    times = document.querySelectorAll(selector)

	time = 0

	for(i=0; i<times.length; i++){
		t = times[i].innerHTML
		time += timeInSeconds(t)
    }
	
	minutes = Math.floor(time / 60)
	hours   = Math.floor(minutes / 60)
	minutes = minutes % 60
	seconds = time % 60
	console.log(hours + ':' + minutes + ':' + seconds)
	return time
}
