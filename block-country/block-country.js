// setup
document.body.style.display = 'none';

// write country full name
let blockedCountries = ['Egypt', 'United States', 'NetherLands'];
// apply the steps that i will apply on other data to check RAW equallity
blockedCountries = blockedCountries.map(country => {
	return country.toLowerCase().replace(/\W/g, '');
});

// helper functions
function randomId(){
	// return random string and make sure it start with string
	let text = 97;
	let flag = true;
	while(flag){
		text = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
		flag = parseInt(text) >= 0;
	}
	return text;
}

function createOutputElement(){
	let id = randomId();
	let elm = document.createElement('div');
	elm.id = id;
	elm.style.display = 'none';
	document.body.appendChild(elm);
	return id;
}

// check the ip geo location
let urlToGetGeoLocation = 'https://ipgeolocation.io/';
let containerId = createOutputElement();
let container = document.getElementById(containerId);
let country;
let contentIfBlocked = '<h1 id="msg-404">404 Error Not Found</h1>' 

fetch(urlToGetGeoLocation)
	.then(function(data) {
		return data.text();
	})
	.then(function(src) {
		container.innerHTML = src;
		country = container.querySelector('#countryName');
		country = country.innerText.replace(/\W/g, '').toLowerCase();
		if(blockedCountries.includes(country)){
			document.body.innerHTML = contentIfBlocked;
		}
	})
	.then(function(src){
		container.innerHTML = '';
		document.body.style.display = '';
	})
