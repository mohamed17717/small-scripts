from scraper import *
from random import randint

website = '.lynda.com'

s = Scraper()
cookies = s.get_cookies(browser='firefox', website=website)
s.set_cookies(cookies)

with open('lyndaVideosList.json') as f:
	videoLinks = loads(f.read())

videos = []
for link in videoLinks:
	s.get(link)
	s.html_soup()

	soup = s.soup
	elm  = soup.video
	videoSrc = unquote(elm['data-src'])

	videos.append(videoSrc)
	print(videoSrc)
	sleep(randint(3, 9))

	

with open('lyndaVideos.json', 'w') as f:
	f.write( dumps(videos) )
