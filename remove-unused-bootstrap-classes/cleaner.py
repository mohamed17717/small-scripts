# media and @ queries not wapeended yet
from bs4 import BeautifulSoup as Soup
from re import match, findall, sub

def readFile(filename):
	with open(filename) as f:
		data = f.read()
		f.close()
	return data
## parse css into selector and props
def css_remove_comments(cssSheet):
	ptrn = r'\/\*[^*]*\*+([^\/\*][^*]*\*+)*\/'
	return sub(ptrn, '',cssSheet)
def parse_css(cssSheet):
	""" 
		take css minified(option) text
		return dict {selector: [rules1, rules2, ...]}
	"""
	cssSheet = css_remove_comments(cssSheet)
	result = {}

	carry = ''
	selector = ''
	count_brackets = 0
	for l in cssSheet:
		if l == '{': 
			count_brackets += 1
			if count_brackets == 1:
				selector = carry
				carry = '{'
				if not result.get(selector):
					result[selector] = []
			else:
				carry += '{'
		elif l == '}':
			count_brackets -= 1
			carry += '}'
			if count_brackets == 0:
				result[selector].append( carry )
				carry = ''
		else:
			carry += l

	return result
def purifyCss(cssSheet, html):
	html = Soup(html, 'html.parser')
	res = parse_css(cssSheet)	
	newCSS =  ''
	
	for key in res.keys() : 
		selectors = key.split(',')
		for selector in selectors:
			flag = False
			if selector.startswith(':'): flag = True
			elif selector.startswith('@'): pass
			else:
				## clean selctor
				splitters = (':' , '+', '>')
				s = selector
				for spliter in splitters:
					s = s.split( spliter )[0]

				flag = html.select_one(s) and True or False
			if flag:
				newCSS += key + key.join(res[key])
				break
	return newCSS


htmlFile = 'index.html'
bootstrapFile = 'bootstrap.min.css'

html = readFile(htmlFile)
bs   = readFile(bootstrapFile)

newCSS = purifyCss(bs, html)
print(newCSS)

''' 
- tag name
- 
'''
