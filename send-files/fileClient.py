import socket
from time import time

recvDir = './RecvPythonTest/'

def main():
    host = '127.0.0.1'
    port = 17717
    server = (host, port)
    
    s = socket.socket()
    s.connect(server)
    
    fileName = input('fileName -> ')
    if fileName != 'q':
        s.send(fileName.encode())
        response = str(s.recv(1024))[2:-1]
        print(response)
        if response[:6] == 'Exists':
            fileSize = int(response[6+1:])
            decide = input('file exist and its size ' + str(fileSize/1024) + 'bytes .. download ?! (Y/N)' )
            if decide[:2] == 'y':
                s.send('ok'.encode())
                recieved = 0

                start = time()
                
                with open(recvDir + 'new_' + fileName, 'wb') as f:
                    while recieved < fileSize:
                        data = s.recv(1024)
                        f.write(data)
                        recieved += len(data)
                print(recieved, ' in ', time() - start)
                    
#                         print((recieved/fileSize) * 100, '% done')
                print('download is complete')
                s.close()
        else:
            print('file is not exist')
            s.close()
    else:
        print('Bye Bye Bitch')
        s.close()
    
if __name__ == '__main__':
    main()
