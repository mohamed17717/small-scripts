import socket
import threading
import os

senderDir = './senderPythonTest/'

def RetrFile(name, sock):
    fileName = str(sock.recv(1024))[2:-1]
    fileName = senderDir + fileName
    
    if os.path.isfile(fileName):
        size = os.path.getsize(fileName)
        sock.send( ('Exists ' + str(size)).encode() )
        userResponse = str(sock.recv(1024))[2:-1]
        if userResponse[:2] == 'ok':
            sended = 0
            with open(fileName, 'rb') as f:
                while sended < size:
                    bytesToSend = f.read(1024)
                    sock.send(bytesToSend)
                    sended += len(bytesToSend)
            print(sended)
    else:
        sock.send('Err'.encode())
        
    sock.close()



def main():
    host = '127.0.0.1'
    port = 17717
    server = (host,port)
    
    s = socket.socket()
    
    s.bind(server)
    
    s.listen(5)
    print('Server Started...')
    
    while True:
        conn, addr = s.accept()
        print('client connected with ip:<%s>' % str(addr))
        
        t = threading.Thread(target = RetrFile, args = ('retrThread', conn))
        t.start()
        
    s.close()
    
if __name__ == '__main__':
    main()
